interface Question {
  question: string;
  choices: string[];
  correctAnswer: number;
}

const questions: Question[] = [
  {
    question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
    choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
    correctAnswer: 0,
  },
  {
    question: 'Which method is used to add one or more elements to the end of an array?',
    choices: ['push()', 'join()', 'slice()', 'concat()'],
    correctAnswer: 0,
  },
  {
    question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
    choices: ['"327"', '"12"', '"57"', '"NaN"'],
    correctAnswer: 2,
  },
  {
    question: 'What is the purpose of the "use strict" directive in JavaScript?',
    choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
    correctAnswer: 2,
  },
  {
    question: 'What is the scope of a variable declared with the "let" keyword?',
    choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
    correctAnswer: 2,
  },
  {
    question: 'Which higher-order function is used to transform elements of an array into a single value?',
    choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
    correctAnswer: 2,
  },
  {
    question: 'What does the "=== " operator in JavaScript check for?',
    choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
    correctAnswer: 1,
  },
  {
    question: 'What is the purpose of the "this" keyword in JavaScript?',
    choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
    correctAnswer: 3,
  },
  {
    question: 'What does the "NaN" value represent in JavaScript?',
    choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
    correctAnswer: 0,
  },
  {
    question: 'Which method is used to remove the last element from an array?',
    choices: ['pop()', 'shift()', 'slice()', 'splice()'],
    correctAnswer: 0,
  },
];

const div = document.getElementById("exercise2") as HTMLDListElement
const score = document.createElement("div");
score.innerText = "Current Score: 0/10";
div.appendChild(score);

let count = 0;

for (const { question: q, choices: c, correctAnswer: b } of questions) { // loop วนแสดงคำถาม , choices, คำตอบ
  const div2 = document.createElement("div")
  const question2 = document.createElement("p")
  const div4 = document.createElement("div")
  const button = document.createElement("button")
  const answer = document.createElement("p");
  question2.innerText = q;
  const input: HTMLInputElement[] = [];
  let ans: number // เก็บค่าคำตอบ

  for (let i = 0; i < c.length; i++) { // loop วนแสดง choices
    const div = document.createElement("div")
    const radio = document.createElement("input")
    radio.type = "radio"
    const span = document.createElement("span")
    span.innerText = c[i]
    button.innerText = "submit"

    // ฟังก์ชัน เมื่อคลิกแล้วสลับตัวเลือก radio 
    radio.addEventListener("click", () => {
      for (const change of input) {
        change.checked = false
        radio.checked = true
        ans = i;
      }
    })

    input.push(radio)
    div.appendChild(radio)
    div.appendChild(span)
    div4.appendChild(div)
  }

  // ฟังก์ชันเมื่อคลิกปุ่ม submit 
  button.addEventListener("click", () => {
    
  // เทียบค่าคำตอบกับคำตอบที่ถูกต้อง
    if (ans == null) {
      return alert("please choose an answer first!")
    } else if (ans == b) {
      answer.innerText = "Correct!"
      count++;
      score.innerText = "Current Score: " + count + "/10";
    } else {
      answer.innerText = "Incorrect!"
    }

  // ปิดการใช้งานตัวเลือกทั้งหมด
    for (const radio of input) {
      radio.disabled = true;
    }
  // ปิดการใช้งานปุ่ม Submit
    button.disabled = true;
  
  })
  div2.appendChild(question2)
  div2.appendChild(div4)
  div2.appendChild(button)
  div2.appendChild(answer)
  div.appendChild(div2)
}